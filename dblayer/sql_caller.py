from sqlalchemy import create_engine, update
from sqlalchemy.orm import sessionmaker
import pandas as pd
import time
from dblayer import models
import datetime

class SqlCaller():
    """
    SqlCaller() --> This module is meant to dump various data into a database.
    Must instantiate SqlCaller() with census api key and db connection string

    engine_string: this is the db string to use

    Parameters:
        create_tables: Set to true on first run to create tables.
    """

    def __init__(self, create_tables=False):
        engine_string = 'mysql+pymysql://jtoh_89:jack1ass@macro.cpuughirda5i.us-east-1.rds.amazonaws.com:3306/economicdata'
        self.engine = create_engine(engine_string)

        if create_tables:
            print("Creating tables")
            models.InitiateDeclaratives.create_tables(engine_string)

    def db_dump_props(self, df, source=''):
        """
        db_dump_props() --> takes in dataframe and dumps to Properties table

        Parameters:
            df: pass in dataframe
        """
        start = time.time()
        if source == 'mls':
            df.to_sql('MLS_Properties', if_exists='append', con=self.engine, index=False)
        else:
            df.to_sql('Properties', if_exists='append', con=self.engine, index=False)
        end = time.time()
        print('Dumped {} listings. Write to sql took: {}'.format(len(df), end-start))

    def db_dump_rentometer_rents(self, df):
        """
        db_dump_rents() --> takes in dataframe and dumps to RentOmeter table

        Parameters:
            df: pass in dataframe
        """

        start = time.time()
        df.to_sql('RentOMeter', if_exists='append', con=self.engine, index=False)
        end = time.time()
        print('Dumped {} listings. Write to sql took: {}'.format(len(df), end-start))

    def get_rentometer_db(self):
        query = 'select address, PropertyHashID, Beds, Rent from RentOMeter'
        df = pd.read_sql_query(query, self.engine)

        return df


    def get_all_properties(self, source='',market=None):
        '''
            get_MLS_Properties() --> gets all from MLS_Properties
            get_Properties_mls_sourced --> gets all from Properties where Source = 'mls'
            get_nonMLS_Properties() --> gets all from Properties where Source != 'mls'
        '''


        if source == 'mls':
            query = 'Call get_MLS_Properties(\'' + market + '\')'
            df = pd.read_sql_query(query, self.engine)
        elif source == 'prop_mls_sourced':
            query = 'Call get_Properties_mls_sourced()'
            df = pd.read_sql_query(query, self.engine)
        elif source == 'nonmls':
            query = 'Call get_nonMLS_Properties()'
            df = pd.read_sql_query(query, self.engine)
        else:
            query = 'Call get_all_Properties()'
            df = pd.read_sql_query(query, self.engine)

        return df

    def get_Active_MLS(self):
        '''
            get_active_mls() --> gets all active in MLS_Properties
        '''
        query = 'Call get_active_mls()'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_rentometer_rents(self, PropertyHashIDs):

        Session = sessionmaker(bind=self.engine)
        session = Session()
        query = session.query(models.RentOMeter.PropertyHashID, models.RentOMeter.Beds, models.RentOMeter.Rent)\
            .filter(models.RentOMeter.PropertyHashID.in_(PropertyHashIDs))
        df = pd.read_sql(query.statement, self.engine)
        return df

    def db_update_rents(self, new_rent, table):
        """
        db_dump_prop_images() --> takes in list of properties no longer available and updates as off market

        Parameters:
            list: list of propertyids
        """

        if table == 'rentometer':
            new_rent.to_sql('temp_rentallistings_update', self.engine, if_exists='replace')

            sql = """
                UPDATE RentOMeter p
                INNER JOIN temp_rentallistings_update u ON p.Address = u.address and p.Beds = u.Beds
                SET p.Rent = u.rent
                WHERE p.Address = u.address and p.Beds = u.Beds;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                print('Updated {} rental listings'.format(len(new_rent)))
                conn.execute(sql)
        elif table == 'mls':
            new_rent.to_sql('temp_mlsrent_update', self.engine, if_exists='replace')

            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_mlsrent_update u ON p.PropertyHashID = u.PropertyHashID and p.Beds = u.Beds
                SET p.Rent = u.rent_y
                WHERE p.PropertyHashID = u.PropertyHashID and p.Beds = u.Beds;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                print('Updated {} rental listings'.format(len(new_rent)))
                conn.execute(sql)
    def get_active_properties(self):
        '''
            select Address,City,State,Zip,Beds,Baths,YearBuilt,Price,Sqft,PropertyHashID,URL, Market, Source, Status
            from Properties
            where Status = "active"
        '''
        query = 'Call get_Properties()'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_new_properties_esri(self):
        '''

        '''
        query = 'Call get_new_props()'
        df = pd.read_sql_query(query, self.engine)

        return df


    def get_new_listings(self):
        '''

        '''
        query = 'Call get_Properties()'
        df = pd.read_sql_query(query, self.engine)

        return df


    def db_update_properties(self, updatetype, df):
        """
        db_dump_prop_images() --> takes in list of properties no longer available and updates as off market

        Parameters:
            list: list of propertyids
        """

        if updatetype == 'status':
            df.to_sql('temp_status_update', self.engine, if_exists='replace')

            sql = """
                UPDATE Properties p
                INNER JOIN temp_status_update u ON p.PropertyHashID = u.PropertyHashID
                SET Status = u.Status_x, p.UpdateDate = CURDATE(), p.Source = u.Source_x
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                print('Updated status for {} listings'.format(len(df)))
                conn.execute(sql)
        elif updatetype == 'price':
            df.to_sql('temp_price_update', self.engine, if_exists='replace')

            sql = """
                UPDATE Properties p
                INNER JOIN temp_price_update u ON p.PropertyHashID = u.PropertyHashID
                SET Price = u.Price_x, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated price for {} listings'.format(len(df)))
        elif updatetype == 'status_inactive':
            df.to_sql('temp_status_inactive', self.engine, if_exists='replace')
            sql = """
                UPDATE Properties p
                INNER JOIN temp_status_inactive u ON p.PropertyHashID = u.PropertyHashID
                SET p.Status = 'Inactive', p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated inactive status for {} listings'.format(len(df)))
        elif updatetype == 'rent':
            df.to_sql('temp_rent', self.engine, if_exists='replace')
            sql = """
                UPDATE Properties p
                INNER JOIN temp_rent u ON p.PropertyHashID = u.PropertyHashID
                SET p.Rent = u.Rent_x, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated rent for {} listings'.format(len(df)))
        elif updatetype == 'esri_analyzed':
            df.to_sql('temp_esri_analyzed', self.engine, if_exists='replace')

            sql = """
                        UPDATE Properties p
                        INNER JOIN temp_esri_analyzed u ON p.PropertyHashID = u.PropertyHashID
                        SET p.ESRI_Analyzed = 1, p.UpdateDate = CURDATE()
                        WHERE p.PropertyHashID = u.PropertyHashID;
                    """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
        elif updatetype == 'gooddeal':
            df.to_sql('temp_esri_good', self.engine, if_exists='replace')

            sql = """
                        UPDATE Properties p
                        INNER JOIN temp_esri_good u ON p.PropertyHashID = u.PropertyHashID
                        SET p.GoodDeal = 1, p.UpdateDate = CURDATE()
                        WHERE p.PropertyHashID = u.PropertyHashID;
                    """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)

    def db_update_mls_properties(self, updatetype, df):
        """
        db_dump_prop_images() --> takes in list of properties no longer available and updates as off market

        Parameters:
            list: list of propertyids
        """

        if updatetype == 'status':
            df.to_sql('temp_status_update', self.engine, if_exists='replace')

            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_status_update u ON p.PropertyHashID = u.PropertyHashID
                SET Status = u.Status_x, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated status for {} listings'.format(len(df)))
        elif updatetype == 'price':
            df.to_sql('temp_price_update', self.engine, if_exists='replace')

            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_price_update u ON p.PropertyHashID = u.PropertyHashID
                SET Price = u.Price_x, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated price for {} listings'.format(len(df)))
        elif updatetype == 'status_inactive':
            df.to_sql('temp_status_inactive', self.engine, if_exists='replace')
            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_status_inactive u ON p.PropertyHashID = u.PropertyHashID
                SET p.Status = 'Inactive', p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated inactive status for {} listings'.format(len(df)))
        elif updatetype == 'rent':
            df.to_sql('temp_rent', self.engine, if_exists='replace')
            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_rent u ON p.PropertyHashID = u.PropertyHashID
                SET p.Rent = u.Rent, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated rent for {} listings'.format(len(df)))
        elif updatetype == 'rentprice':
            df.to_sql('temp_rentprice', self.engine, if_exists='replace')
            sql = """
                UPDATE MLS_Properties p
                INNER JOIN temp_rentprice u ON p.PropertyHashID = u.PropertyHashID
                SET p.RentToPriceRatio = u.RentToPriceRatio, p.UpdateDate = CURDATE()
                WHERE p.PropertyHashID = u.PropertyHashID;
            """
            with self.engine.begin() as conn:  # TRANSACTION
                conn.execute(sql)
                print('Updated rentprice for {} listings'.format(len(df)))


    def get_msa_names(self):
        Session = sessionmaker(bind=self.engine)
        session = Session()

        response = session.query(models.MSA.MSA_ID, models.MSA.MSA_Name).distinct().all()

        msa_names_dict = dict(response)

        return msa_names_dict

    def get_msa_counties(self):
        Session = sessionmaker(bind=self.engine)
        session = Session()

        response = session.query(models.MSA_Counties.County_UID, models.MSA_Counties.MSA_ID).distinct().all()

        msa_counties_dict = dict(response)

        return msa_counties_dict

    def get_population(self, msaid):
        query = 'Call get_population(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_census_profiles(self, msaid):
        query = 'Call get_census_profiles(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_rents(self, msaid):
        query = 'Call get_rents(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_affordability(self, msaid):
        query = 'Call get_affordability(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_dom(self, msaid):
        query = 'Call get_dom(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_householdmedianincome(self, msaid):
        query = 'Call get_householdmedianincome(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_mediansales(self, msaid):
        query = 'Call get_mediansales(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_mediansalesyoy(self, msaid):
        query = 'Call get_mediansalesyoy(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_monthsupply(self, msaid):
        query = 'Call get_monthsupply(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        df['MonthsOfSupply'] = df['MonthsOfSupply'].astype(float)

        return df

    def get_pricecuts(self, msaid):
        query = 'Call get_pricecuts(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_zhvi(self, msaid):
        query = 'Call get_zhvi(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_unemployment(self, msaid):
        query = 'Call get_unemployment(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_industry(self, msaid):
        query = 'Call get_industry(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df

    def get_buildingpermits(self, msaid):
        query = 'Call get_buildingpermits(\'' + msaid + '\')'
        df = pd.read_sql_query(query, self.engine)

        return df