from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, MetaData, create_engine, Column, Integer, \
    String, Float, DateTime, BigInteger, Boolean, Date, SmallInteger


Base = declarative_base()


class MSA(Base):
    __tablename__ = "MSA"

    MSA_UID = Column(Integer, unique=True, primary_key=True)
    MSA_ID = Column(Integer, unique=False)
    MSA_Name = Column(String(250), unique=False)
    State_ID = Column(String(2), unique=False)

class MSA_Counties(Base):
    __tablename__ = "MSA_Counties"

    MSA_Counties_UID = Column(Integer, unique=True, primary_key=True)
    MSA_ID = Column(Integer, unique=False)
    MSA_Name = Column(String(250), unique=False)
    County_UID = Column(String(6), unique=False)
    County_ID = Column(String(3), unique=False)
    State_ID = Column(String(2), unique=False)

class Properties(Base):
    __tablename__ = "Properties"

    PropertyHashID = Column(String(100), unique=True, primary_key=True, autoincrement=False)
    CapRate = Column(Float, unique=False)
    Address = Column(String(250), unique=False)
    City = Column(String(100), unique=False)
    State = Column(String(3), unique=False)
    Zip = Column(Integer, unique=False)
    Market = Column(String(100), unique=False)
    Beds = Column(Integer, unique=False)
    Baths = Column(Float, unique=False)
    YearBuilt = Column(Integer, unique=False)
    Price = Column(Integer, unique=False)
    Sqft = Column(Integer, unique=False)
    Rent = Column(Integer, unique=False)
    RentToPriceRatio = Column(Float, unique=False)
    Downpayment = Column(Float, unique=False)
    MortgageInterest = Column(Float, unique=False)
    MortgagePayment = Column(Float, unique=False)
    Insurance = Column(Float, unique=False)
    Taxes = Column(Float, unique=False)
    PropertyManagement = Column(Float, unique=False)
    Hoa = Column(Float, unique=False)
    Vacancy = Column(Float, unique=False)
    Maintenance = Column(Float, unique=False)
    MonthlyCashflow = Column(Integer, unique=False)
    Class = Column(String(1), unique=False)
    PropertyType = Column(String(50), unique=False)
    Turnkey = Column(String(3), unique=False)
    Status = Column(String(10), unique=False)
    Source = Column(String(100), unique=False)
    SellersEmail = Column(String(150), unique=False)
    MSA_ID = Column(Integer, unique=False)
    MSA_Name = Column(String(250), unique=False)
    State_ID = Column(Integer, unique=False)
    County_UID = Column(Integer, unique=False)
    Tract_ID = Column(Integer, unique=False)
    x_coordinate = Column(Float, unique=False)
    y_coordinate = Column(Float, unique=False)
    PriceSqft = Column(Integer, unique=False)
    DaysOnMarket = Column(Integer, unique=False)
    UpdateDate = Column(DateTime, unique=False)
    URL = Column(String(500), unique=False)
    AddDate = Column(DateTime, unique=False)
    ESRI_OwnerOccupiedRates = Column(Float, unique=False)
    ESRI_MedianHouseholdIncome = Column(Integer, unique=False)
    ESRI_PersonalCrimeIndex = Column(Integer, unique=False)
    ESRI_Analyzed = Column(Boolean, unique=False)
    GoodDeal = Column(Boolean, unique=False)


class MLS_Properties(Base):
    __tablename__ = "MLS_Properties"
    PropertyHashID = Column(String(100), unique=True, primary_key=True, autoincrement=False)
    CapRate = Column(Float, unique=False)
    Address = Column(String(250), unique=False)
    City = Column(String(100), unique=False)
    State = Column(String(3), unique=False)
    Zip = Column(Integer, unique=False)
    Market = Column(String(100), unique=False)
    Beds = Column(Integer, unique=False)
    Baths = Column(Float, unique=False)
    YearBuilt = Column(Integer, unique=False)
    Price = Column(Integer, unique=False)
    Sqft = Column(Integer, unique=False)
    Rent = Column(Integer, unique=False)
    RentToPriceRatio = Column(Float, unique=False)
    Downpayment = Column(Float, unique=False)
    MortgageInterest = Column(Float, unique=False)
    MortgagePayment = Column(Float, unique=False)
    Insurance = Column(Float, unique=False)
    Taxes = Column(Float, unique=False)
    PropertyManagement = Column(Float, unique=False)
    Hoa = Column(Float, unique=False)
    Vacancy = Column(Float, unique=False)
    Maintenance = Column(Float, unique=False)
    MonthlyCashflow = Column(Integer, unique=False)
    Class = Column(String(1), unique=False)
    PropertyType = Column(String(50), unique=False)
    Turnkey = Column(String(3), unique=False)
    Status = Column(String(10), unique=False)
    Source = Column(String(100), unique=False)
    SellersEmail = Column(String(150), unique=False)
    MSA_ID = Column(Integer, unique=False)
    MSA_Name = Column(String(250), unique=False)
    State_ID = Column(Integer, unique=False)
    County_UID = Column(Integer, unique=False)
    Tract_ID = Column(Integer, unique=False)
    x_coordinate = Column(Float, unique=False)
    y_coordinate = Column(Float, unique=False)
    PriceSqft = Column(Integer, unique=False)
    DaysOnMarket = Column(Integer, unique=False)
    UpdateDate = Column(Date, unique=False)
    URL = Column(String(500), unique=False)
    AddDate = Column(Date, unique=False)

class RentOMeter(Base):
    __tablename__ = "RentOMeter"
    Rent_ID = Column(Integer, unique=True, primary_key=True)
    PropertyHashID = Column(String(100), unique=False)
    Address = Column(String(150), unique=False)
    Status = Column(String(10), unique=False)
    Report_time = Column(Date, unique=False)
    Beds = Column(Integer, unique=False)
    Rent = Column(Integer, unique=False)
    Median = Column(Integer, unique=False)
    Min = Column(Integer, unique=False)
    Max = Column(Integer, unique=False)
    Std_dev = Column(Float, unique=False)
    Radius = Column(Integer, unique=False)
    Sample_size = Column(Integer, unique=False)


class InitiateDeclaratives():
    @staticmethod
    def create_tables(engine_string):
        engine = create_engine(engine_string)
        Base.metadata.create_all(engine)