from properties import Gsheet_parser
from msa_reports import run_macro_reports
from properties import MLS_parser
from dblayer import sql_caller
from properties import property_parser
from properties import rentometer_parser
from properties import property_rent_value_filter
# sql_caller.SqlCaller(create_tables=True)



######################################################################################
#                 GOOGLE SHEET PROPERTIES PROCESS
######################################################################################

sql_caller.SqlCaller(create_tables=True)

#  Run Properties on GSheet
# gsheetprops = Gsheet_parser.GSheetProperties()
# gsheetprops.run_property_conversion(update_db=True)



######################################################################################
#                 MLS PROPERTIES PROCESS
######################################################################################

mls = MLS_parser.Redfin_Parser()

#     **** Run MLS Properties Process ****
#      1-Run saved search on Redfin and download file.
#      2-Rename file according to market and replace existing file in redfin_mls.
#      3-Run process_MLS_props())


# mls.process_MLS_props(update_db=True)


#     **** RUN RENTS FOR MLS WITHOUT RENT VALUES ****
#      1-Extract MLS properties in MySQL without rent values
#      2-Run batch analysis on RentOMeter
#      3-Save file in rentometer folder. Replace existing
#      4-Run store_rents(). This will store RentOMeter in db
#      5-Run assign_rents(). This will assign any rents for address in RentOMeter report
#      6 - Run filter_MLS_rent_price().


# rentometer_parser.Rentometer().store_rents()
#
# rentometer_parser.Rentometer().assign_rents()

# property_rent_value_filter.RentPriceFilter().dump_decent_mls_props()




#     **** ENERATE UPDATED MLS LISTINGS LIST ****
#      1-Run generate_Pre_ESRI() to generate Pre_ESRI.xlsx to import into ESRI.

property_parser.PropertyParser().generate_Pre_ESRI()



###         Step 4 - Filter Good Listings         ####
#   1 - Select all sites on ESRI and generate comparison report
#   2 - Save ESRI report as csv and Run this script

# property_parser.PropertyParser().filter_ESRI_analysis()






# # # #   Run Custom MSA Report
# run_macro_reports.run_select_msas('1, 28140, 26420, 19100, 12420, 26900, 32820, 16980, 19430, 41180, 12060,13820, 17140, 17460, 27140')

