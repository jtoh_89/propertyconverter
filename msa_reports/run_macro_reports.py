from properties import Gsheet_parser
from dblayer import sql_caller
import pandas as pd
import os


def run_macro_reports(props, only_new_markets=True):
    sql = sql_caller.SqlCaller(create_tables=False)


    path = os.path.dirname(os.path.abspath(__file__))

    final_df = pd.DataFrame()

    props = props.dropna(subset=['MSA_ID'])
    used_msaids = props[['MSA_ID','MSA_Name','Market']].drop_duplicates()


    if only_new_markets:
        existing_msa = []
        for filename in os.listdir(path):
            if 'DS_Store' in filename:
                continue

            for i, msa in used_msaids.iterrows():
                if msa['Market'] in filename:
                    print('Already have Report for {}'.format(msa['Market']))
                    existing_msa.append(msa['Market'])
                    continue

        if len(existing_msa) > 0:
            used_msaids = used_msaids[~used_msaids['Market'].isin(existing_msa)]

    #   get usa
    US_zhvi_df = sql.get_zhvi(msaid='1')
    US_pricecuts_df = sql.get_pricecuts(msaid='1')
    US_unemployment_df = sql.get_unemployment(msaid='1')
    US_rents_df = sql.get_rents(msaid='1')
    US_affordability_df = sql.get_affordability(msaid='1')
    US_mediansalesyoy_df = sql.get_mediansalesyoy(msaid='1')
    US_housholdmedianincome = sql.get_householdmedianincome(msaid='1')



    for index, row in used_msaids.iterrows():
        print('Running report for MSA: {}'.format(row['Market']))

        population_df = sql.get_population(msaid=row['MSA_ID'])
        zhvi_df = sql.get_zhvi(msaid=row['MSA_ID'])
        pricecuts_df = sql.get_pricecuts(msaid=row['MSA_ID'])
        unemployment_df = sql.get_unemployment(msaid=row['MSA_ID'])
        rents_df = sql.get_rents(msaid=row['MSA_ID'])
        affordability_df = sql.get_affordability(msaid=row['MSA_ID'])
        dom_df = sql.get_dom(msaid=row['MSA_ID'])
        mediansales_df = sql.get_mediansales(msaid=row['MSA_ID'])
        mediansalesyoy_df = sql.get_mediansalesyoy(msaid=row['MSA_ID'])
        monthsupply_df = sql.get_monthsupply(msaid=row['MSA_ID'])
        householdmedianincome_df = sql.get_householdmedianincome(msaid=row['MSA_ID'])

        mediansales_sumdf = mediansales_df
        mediansalesyoy_sumdf = mediansalesyoy_df
        unemployment_sumdf = unemployment_df
        householdmedianincome_sumdf = householdmedianincome_df




    #   Comparison to US
        zhvi_df = pd.concat([US_zhvi_df, zhvi_df])
        pricecuts_df = pd.concat([US_pricecuts_df, pricecuts_df])
        unemployment_df = pd.concat([US_unemployment_df, unemployment_df])
        rents_df = pd.concat([US_rents_df, rents_df])
        affordability_df = pd.concat([US_affordability_df, affordability_df])
        mediansalesyoy_df = pd.concat([US_mediansalesyoy_df, mediansalesyoy_df])
        householdmedianincome_df = pd.concat([US_housholdmedianincome, householdmedianincome_df])


    #   Get Macro Summary
        mediansales_df['Date'] = pd.to_datetime(mediansales_df['Date']).dt.date
        mediansalesyoy_df['Date'] = pd.to_datetime(mediansalesyoy_df['Date']).dt.date
        unemployment_df['Date'] = pd.to_datetime(unemployment_df['Date']).dt.date
        monthsupply_df['Date'] = pd.to_datetime(monthsupply_df['Date']).dt.date

        try:
            unemployment = unemployment_sumdf.loc[unemployment_sumdf["Year"] == max(unemployment_sumdf['Year'])]
        except:
            unemployment = pd.DataFrame.from_dict({'Unemployment_rate':['N/A'],'Date':['N/A']})

        try:
            mediansalesprice = mediansales_sumdf.loc[pd.to_datetime(mediansales_sumdf['Date']).dt.date == max(pd.to_datetime(mediansales_sumdf['Date']).dt.date)]
        except:
            mediansalesprice = pd.DataFrame.from_dict({'MedianSalePrice':['N/A'],'Date':['N/A']})

        try:
            mediansalespriceYoy = mediansalesyoy_sumdf.loc[pd.to_datetime(mediansalesyoy_sumdf['Date']).dt.date
                                                           == max(pd.to_datetime(mediansalesyoy_sumdf['Date']).dt.date)]
        except:
            mediansalespriceYoy = pd.DataFrame.from_dict({'MedianSalePriceYoy':['N/A'],'Date':['N/A']})

        try:
            monthsupply = monthsupply_df.loc[pd.to_datetime(monthsupply_df['Date']).dt.date == max(pd.to_datetime(monthsupply_df['Date']).dt.date)]
        except:
            monthsupply = pd.DataFrame.from_dict({'MonthsOfSupply':['N/A'],'Date':['N/A']})

        try:
            householdmedianincome = householdmedianincome_sumdf.loc[householdmedianincome_sumdf["Year"] == max(householdmedianincome_sumdf['Year'])]
        except:
            householdmedianincome = pd.DataFrame.from_dict({'HouseholdMedianIncome':['N/A'],'Date':['N/A']})

        summary_df = pd.DataFrame(columns=['Summary','Value','Date'])
        summary_df = summary_df.append(pd.DataFrame({'Value':unemployment['Unemployment_rate'],'Summary':'UnemploymentRate','Date':unemployment['Year']}), sort=False)
        summary_df = summary_df.append(pd.DataFrame({'Value':mediansalesprice['MedianSalePrice'],'Summary':'MedianSalesPrice','Date':mediansalesprice['Date']}), sort=False)
        summary_df = summary_df.append(pd.DataFrame({'Value':mediansalespriceYoy['MedianSalePriceYoy'],'Summary':'MedianSalesPriceYoy','Date':mediansalespriceYoy['Date']}), sort=False)
        summary_df = summary_df.append(pd.DataFrame({'Value':monthsupply['MonthsOfSupply'],'Summary':'MonthsOfSupply','Date':monthsupply['Date']}), sort=False)
        summary_df = summary_df.append(pd.DataFrame({'Value':householdmedianincome['HouseholdMedianIncome'],'Summary':'HouseholdMedianIncome','Date':householdmedianincome['Year']}), sort=False)

    #   Group by Year
        zhvi_df = zhvi_df.groupby(['MSA_ID','MSA_Name','Year','bedrooms']).mean().reset_index()
        dom_df = dom_df.groupby(['MSA_ID','MSA_Name','Year']).mean().reset_index()
        pricecuts_df = pricecuts_df.groupby(['MSA_ID','MSA_Name','Tier','Year']).mean().reset_index()
        unemployment_df = unemployment_df.groupby(['MSA_ID','MSA_Name','Year']).mean().reset_index()
        rents_df = rents_df.groupby(['MSA_ID','MSA_Name','Year','bedrooms']).mean().reset_index()
        affordability_df = affordability_df.groupby(['MSA_ID','MSA_Name','DataType','Year']).mean().reset_index()
        monthsupply_df = monthsupply_df.groupby(['MSA_ID','MSA_Name','Year']).mean().reset_index()


        industry_df = sql.get_industry(msaid=row['MSA_ID'])
        buildingpermits_df = sql.get_buildingpermits(msaid=row['MSA_ID'])
        buildingpermits_df = buildingpermits_df.rename(
            columns={'Units_1': 'Building Permits (1 Units)', 'Units_2': 'Building Permits (2 Units)', 'Units_3to4': 'Building Permits (3-4 Units)',
                     'Units_5plus': 'Building Permits (5+ Units)'})

    #   Write report
        path = os.path.dirname(os.path.abspath(__file__))
        writer = pd.ExcelWriter(os.path.join(path, '{}_MarketReport.xlsx'.format(row['Market'])))

        population_df.to_excel(writer, 'Population', index=False)
        zhvi_df.to_excel(writer, 'ZillowHomeValueIndex', index=False)
        dom_df.to_excel(writer, 'DaysOnMarket', index=False)
        pricecuts_df.to_excel(writer, 'PriceCuts', index=False)
        unemployment_df.to_excel(writer, 'Unemployment', index=False)
        rents_df.to_excel(writer, 'Rents', index=False)
        affordability_df.to_excel(writer, 'Affordability', index=False)
        monthsupply_df.to_excel(writer, 'MonthSupply', index=False)
        industry_df.to_excel(writer, 'Industry', index=False)
        buildingpermits_df.to_excel(writer, 'BuildingPermits', index=False)
        householdmedianincome_df.to_excel(writer, 'HouseholdMedianIncome', index=False)
        summary_df.to_excel(writer, 'MSA_Summary', index=False)

        writer.save()

        print('Finished writing excel reports for {}'.format(row['Market']))




def run_select_msas(msa):
    sql = sql_caller.SqlCaller()


    #   get msas
    population_df = sql.get_population(msaid=msa)
    population_df = population_df.drop(columns=['MSA_ID'])
    population_df['DataType'] = 'Population'
    population_df = population_df.rename(columns={'total_population':'Value'})
    population_df = population_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    zhvi_df = sql.get_zhvi(msaid=msa)
    zhvi_df['DataType'] = 'MedianZHVI (' + zhvi_df['bedrooms'].astype(str) + ' bed)'
    zhvi_df = zhvi_df.drop(columns=['MSA_ID','bedrooms','Date'])
    zhvi_df = zhvi_df.rename(columns={'MedianZHVI':'Value'})
    zhvi_df = zhvi_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    pricecuts_df = sql.get_pricecuts(msaid=msa)
    pricecuts_df['DataType'] = 'PriceCutPercentage (' + pricecuts_df['Tier'].astype(str) + ' tier)'
    pricecuts_df = pricecuts_df.drop(columns=['MSA_ID','Tier','Date'])
    pricecuts_df = pricecuts_df.rename(columns={'PriceCutPercentage':'Value'})
    pricecuts_df = pricecuts_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    unemployment_df = sql.get_unemployment(msaid=msa)
    unemployment_df['DataType'] = 'Unemployment'
    unemployment_df = unemployment_df.drop(columns=['MSA_ID','Date'])
    unemployment_df = unemployment_df.rename(columns={'Unemployment_rate':'Value'})
    unemployment_df = unemployment_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    rents_df = sql.get_rents(msaid=msa)
    rents_df['DataType'] = 'MedianRent (' + rents_df['bedrooms'].astype(str) + ' bed)'
    rents_df = rents_df.drop(columns=['MSA_ID','bedrooms','Date'])
    rents_df = rents_df.rename(columns={'MedianRent':'Value'})
    rents_df = rents_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    affordability_df = sql.get_affordability(msaid=msa)
    affordability_df['DataType'] = 'Affordability (' + affordability_df['DataType'].astype(str) + ')'
    affordability_df = affordability_df.drop(columns=['MSA_ID','HistoricAverage_1985thru1999','Date'])
    affordability_df = affordability_df.rename(columns={'Ratio':'Value'})
    affordability_df = affordability_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    dom_df = sql.get_dom(msaid=msa)
    dom_df['DataType'] = 'DaysOnMarket'
    dom_df = dom_df.drop(columns=['MSA_ID','Date'])
    dom_df = dom_df.rename(columns={'DaysOnMarket':'Value'})
    dom_df = dom_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    mediansales_df = sql.get_mediansales(msaid=msa)
    mediansales_df['DataType'] = 'MedianSalesPrice'
    mediansales_df = mediansales_df.drop(columns=['MSA_ID','Date'])
    mediansales_df = mediansales_df.rename(columns={'MedianSalePrice':'Value'})
    mediansales_df = mediansales_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    mediansalesyoy_df = sql.get_mediansalesyoy(msaid=msa)
    mediansalesyoy_df['DataType'] = 'MedianSalesYoY'
    mediansalesyoy_df = mediansalesyoy_df.drop(columns=['MSA_ID','Date'])
    mediansalesyoy_df = mediansalesyoy_df.rename(columns={'MedianSalePriceYoy':'Value'})
    mediansalesyoy_df = mediansalesyoy_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    monthsupply_df = sql.get_monthsupply(msaid=msa)
    monthsupply_df['DataType'] = 'MonthSupply'
    monthsupply_df = monthsupply_df.drop(columns=['MSA_ID','Date'])
    monthsupply_df = monthsupply_df.rename(columns={'MonthsOfSupply':'Value'})
    monthsupply_df = monthsupply_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    householdmedianincome_df = sql.get_householdmedianincome(msaid=msa)
    householdmedianincome_df['DataType'] = 'HouseholdMedianIncome'
    householdmedianincome_df = householdmedianincome_df.drop(columns=['MSA_ID'])
    householdmedianincome_df = householdmedianincome_df.rename(columns={'HouseholdMedianIncome':'Value'})
    householdmedianincome_df = householdmedianincome_df.groupby(['DataType', 'MSA_Name', 'Year']).mean().reset_index()

    industry_df = sql.get_industry(msaid=msa)
    industry_df = pd.melt(industry_df, id_vars=['MSA_ID', 'MSA_Name', 'Year'])
    industry_df = industry_df.drop(columns=['MSA_ID'])
    industry_df = industry_df.rename(columns={'variable':'DataType','value':'Value'})

    buildingpermits_df = sql.get_buildingpermits(msaid=msa)
    buildingpermits_df = buildingpermits_df.rename(
        columns={'Units_1': 'Building Permits (1 Units)', 'Units_2': 'Building Permits (2 Units)',
                 'Units_3to4': 'Building Permits (3-4 Units)',
                 'Units_5plus': 'Building Permits (5+ Units)'})
    buildingpermits_df = pd.melt(buildingpermits_df, id_vars=['MSA_ID', 'MSA_Name', 'Year'])
    buildingpermits_df = buildingpermits_df.drop(columns=['MSA_ID'])
    buildingpermits_df = buildingpermits_df.rename(columns={'variable':'DataType','value':'Value'})


    macro_report = pd.concat([population_df, zhvi_df, pricecuts_df, unemployment_df, rents_df,
                              affordability_df, dom_df, mediansales_df, mediansalesyoy_df, monthsupply_df,
                              householdmedianincome_df, industry_df, buildingpermits_df], ignore_index=True)

#   Write report
    path = os.path.dirname(os.path.abspath(__file__))
    writer = pd.ExcelWriter(os.path.join(path, 'ManualReports.xlsx'))

    macro_report.to_excel(writer, 'MacroData', index=False)

    writer.save()

    print('Finished manual msa report')


