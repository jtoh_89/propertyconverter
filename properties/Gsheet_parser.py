import censusgeocode as cg
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from dblayer import sql_caller
import hashlib
from mortgage import Loan
import datetime
import sys
from msa_reports import run_macro_reports
from properties import property_parser


class GSheetProperties():

    def run_property_conversion(self, update_db=True):

    #   Retrieve listings
        scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
        creds = ServiceAccountCredentials.from_json_keyfile_name("creds2.json", scope)
        client = gspread.authorize(creds)
        sheet = client.open("Properties").sheet1
        # Open the spreadsheet
        data = sheet.get_all_records()  # Get a list of all records
        df = pd.DataFrame(data)

    #   Drop rows with no address
        df = df[df['Address'].str.len() > 1]


    #   Set property hash
        df['GsheetPropertyHashID'] = (df['Address'] + df['City'] + df['State'])\
            .apply(lambda x: hashlib.md5(x.encode()).hexdigest())

        df['Price'] = df['Price'].astype(str).map(lambda x: x.replace(',', ''))
        df['Sqft'] = df['Sqft'].astype(str).map(lambda x: x.replace(',', ''))
        df['Rent'] = df['Rent'].astype(str).map(lambda x: x.replace(',', ''))
        df['MonthlyCashflow'] = df['MonthlyCashflow'].astype(str).map(lambda x: x.replace(',', ''))

        if update_db:
            df = property_parser.PropertyParser().run_audit(new_df=df, audit_type='nonmls')

        if df.empty:
            print('!!!No new Properties!!!')
            sys.exit()

        #   Normalize Street Address
        df = property_parser.PropertyParser().normalize_address(df)

        df = property_parser.PropertyParser.run_property_conversion(df)

        df = df.rename(columns={'MonthlyTaxes':'Taxes','MonthlyInsurance':'Insurance'})


        #   save new properties and run macroreport for new markets

        if 'x_coordinate' in df.columns:
            df = df[['CapRate','Address','City','State','Zip','Market','Beds','Baths','YearBuilt','Price','Sqft',
                     'Rent','RentToPriceRatio','Downpayment','MortgagePayment','Insurance','Taxes','PropertyManagement','Hoa',
                     'Vacancy','Maintenance','Class','PropertyType','Status','Source','SellersEmail','MSA_ID',
                     'MortgageInterest','MSA_Name','State_ID','County_UID','Tract_ID','x_coordinate','y_coordinate',
                     'PropertyHashID','UpdateDate','MonthlyCashflow','PriceSqft']]

            failed_geocoded_props = df[df.MSA_ID.isnull()]

            geocoded_props = df.dropna(subset=['MSA_ID'])
            geocoded_props['AddDate'] = datetime.datetime.now().date()

            run_macro_reports.run_macro_reports(geocoded_props, only_new_markets=True)

            if update_db:
                sql = sql_caller.SqlCaller(create_tables=False)
                sql.db_dump_props(geocoded_props)
        else:
            failed_geocoded_props = df

        writer = pd.ExcelWriter('PropsFailedGeocode.xlsx')
        failed_geocoded_props.to_excel(writer,'Sheet1', index=False)
        writer.save()


        return df










