import os
import censusgeocode as cg
import pandas as pd
from dblayer import sql_caller
import sys
from properties import property_parser
import geocoder
import hashlib

class Rentometer():
    @staticmethod
    def store_rents():
        path = os.path.dirname(os.path.abspath(__file__))
        path = path + '/rentometer'

        for filename in os.listdir(path):
            if 'DS_Store' in filename or '.py' in filename:
                continue

            with open(os.path.join(path, filename)) as file:
                df = pd.read_csv(file)
                df = df[df['bedrooms'].isin([3,4])]
                df = df.rename(columns={'bedrooms':'Beds','mean':'rent'})

                sql = sql_caller.SqlCaller()
                rentometer_in_db = sql.get_rentometer_db()


                if rentometer_in_db.empty:
                    unique_address = pd.DataFrame(df['address'].drop_duplicates())
                else:
                    common = df.merge(rentometer_in_db, on=['address', 'Beds'])

                    if common.empty:
                        unique_address = pd.DataFrame(df['address'].drop_duplicates())
                    else:
                        new_rent = common[common['rent'] != common.Rent.astype(int)]
                        common = common['address'].unique()
                        df = df[(~df.address.isin(common))]
                        unique_address = pd.DataFrame(df['address'].drop_duplicates())

                        if new_rent.empty:
                            print('!!!No new rental listings!!!')
                        else:
                            new_rent = new_rent.drop(columns=['Rent'])
                            sql.db_update_rents(new_rent, table='rentometer')

                geo_dict = {}
                if len(unique_address) > 0:
                    for i, row in unique_address.iterrows():
                        count = 0
                        while count < 2:
                            try:
                                gmap_api = 'AIzaSyDGiyj328qVAsJWBye7MJ-oEDVBkVg1gzU'
                                address = geocoder.google(row['address'], key=gmap_api)

                                geo_dict[row['address']] = hashlib.md5((address.current_result.address.split(',')[0].strip()
                                    + address.current_result.address.split(',')[1].strip()
                                    + address.current_result.address.split(',')[2].split()[0].strip()).encode()).hexdigest()

                                print('Finished hashing address: {}'.format(row['address']))
                                break
                            except Exception:
                                count += 1
                                print('Error geocoding: {}'.format(address.current_result))
                                if count == 1:
                                    print('!!!!!FAIL!!!!!! from geocode data for {}. CHECK RESPONSE DATA'.format(address.current_result))
                                    continue
                                continue


                if not df.empty:
                    df['PropertyHashID'] = df['address'].map(geo_dict)
                    df = df.drop(columns=['eightieth','twentieth']).rename(columns={'bedrooms':'beds'})

                    sql = sql_caller.SqlCaller(create_tables=False)
                    sql.db_dump_rentometer_rents(df)



    @staticmethod
    def assign_rents():
        sql = sql_caller.SqlCaller(create_tables=False)
        active_mls = sql.get_Active_MLS()

        mls_with_rent = active_mls.dropna(subset=['Rent'])
        MLS_missing_rents = active_mls[active_mls['Rent'].isnull()]


        if not mls_with_rent.empty:
            Renometer_rents = sql.get_rentometer_rents(mls_with_rent['PropertyHashID'])
            common = mls_with_rent.merge(Renometer_rents, on=['PropertyHashID', 'Beds'])

            new_rent = common[common.Rent_x != common.Rent_y]
            if new_rent.empty:
                print('!!!No change in rents!!!')
            else:
                sql.db_update_rents(new_rent, table='mls')

        if len(MLS_missing_rents) > 1:
            PropertyHashIDs_list = MLS_missing_rents['PropertyHashID']

            Renometer_rents = sql.get_rentometer_rents(PropertyHashIDs_list)

            if len(MLS_missing_rents) < 1:
                print('No listings without rent')
                sys.exit()

            common = MLS_missing_rents.merge(Renometer_rents, on=['PropertyHashID', 'PropertyHashID'])

            no_match = MLS_missing_rents[~MLS_missing_rents.PropertyHashID.isin(common.PropertyHashID)]

            final_df = common[common.Beds_x == common.Beds_y]
            final_df = final_df[['PropertyHashID', 'Rent_y']]
            final_df = final_df.rename(columns={'Rent_y':'Rent'}).reset_index(drop=True)

            # path = os.path.dirname(os.path.abspath(__file__))
            # writer = pd.ExcelWriter(os.path.join(path, 'testrent.xlsx'))
            # final_df.to_excel(writer, 'testrent', index=False)
            # writer.save()

            if not no_match.empty:
                print('Listings with RentOMeter rents:\n')
                print(no_match)

            if not final_df.empty:
                sql.db_update_mls_properties(updatetype='rent', df=final_df)





