import os
from properties import Gsheet_parser
import censusgeocode as cg
import pandas as pd
from dblayer import sql_caller
import hashlib
import sys
import time
from msa_reports import run_macro_reports
import datetime
from properties import property_parser
from geopy import Nominatim

class Redfin_Parser():

    def process_MLS_props(self, update_db=True):
        path = os.path.dirname(os.path.abspath(__file__))
        path = path + '/redfin_mls'


        for filename in os.listdir(path):
            if 'DS_Store' in filename or '.py' in filename:
                continue

            if 'redfin' in filename:
                with open(os.path.join(path, filename)) as file:
                    df = pd.read_csv(file)

                    df = df.dropna(subset=['DAYS ON MARKET'])

                    df = df[['ADDRESS','CITY','STATE OR PROVINCE','ZIP OR POSTAL CODE','PRICE',
                             'BEDS','BATHS','SQUARE FEET','YEAR BUILT','DAYS ON MARKET','STATUS','PROPERTY TYPE',
                             'URL (SEE http://www.redfin.com/buy-a-home/comparative-market-analysis FOR INFO ON PRICING)',
                             '$/SQUARE FEET','HOA/MONTH']]

                    if 'dallas' in filename:
                        df = df[(df['$/SQUARE FEET'] > 90) | (df['$/SQUARE FEET'] < 150)]
                        market = 'Dallas'
                        df['Market'] = market
                    elif 'kansas' in filename:
                        df = df[(df['$/SQUARE FEET'] > 70) | (df['$/SQUARE FEET'] < 125)]
                        market = 'Kansas City'
                        df['Market'] = market
                    elif 'memphis' in filename:
                        df = df[(df['$/SQUARE FEET'] > 80) | (df['$/SQUARE FEET'] < 120)]
                        market = 'Memphis'
                        df['Market'] = market
                    elif 'indianapolis' in filename:
                        df = df[(df['$/SQUARE FEET'] > 70) | (df['$/SQUARE FEET'] < 125)]
                        market = 'Indianapolis'
                        df['Market'] = market
                    elif 'atlanta' in filename:
                        df = df[(df['$/SQUARE FEET'] > 70) | (df['$/SQUARE FEET'] < 125)]
                        market = 'Atlanta'
                        df['Market'] = market
                    else:
                        continue
                    df = df.rename(columns={'ADDRESS':'Address',
                                            'CITY':'City',
                                            'STATE OR PROVINCE':'State',
                                            'ZIP OR POSTAL CODE':'Zip',
                                            'PRICE':'Price',
                                            'BEDS':'Beds',
                                            'BATHS':'Baths',
                                            'SQUARE FEET':'Sqft',
                                            'YEAR BUILT':'YearBuilt',
                                            'DAYS ON MARKET':'DaysOnMarket',
                                            'STATUS':'Status',
                                            'PROPERTY TYPE':'PropertyType',
                                            '$/SQUARE FEET':'PriceSqft',
                                            'HOA/MONTH':'Hoa',
                                            'URL (SEE http://www.redfin.com/buy-a-home/comparative-market-analysis FOR INFO ON PRICING)':'URL'})


                    #   Drop rows with no address
                    df = df[df['Address'].str.len() > 1]

                    #   Normalize Street Address
                    df = property_parser.PropertyParser().normalize_address(df)

                    #   Set source to MLS
                    df['Source'] = 'mls'

                    #   Set property hash
                    df['PropertyHashID'] = (df['Address'] + df['City'] + df['State'])\
                        .apply(lambda x: hashlib.md5(x.encode()).hexdigest())

                    if update_db:
                        df = property_parser.PropertyParser().run_audit(new_df=df, audit_type='mls', market=market)

                    if df.empty:
                        print('!!!No new Properties!!!')
                        continue
                        
                    df['Downpayment'] = .25
                    df['MortgageInterest'] = 5.5


                    df = property_parser.PropertyParser().run_property_conversion(df)

                    if df.empty:
                        print('!!!No new Properties after geocoding!!!')
                        continue

                    df = self.run_MLS_audit(df)


                    failed_geocoded_props = df[df.MSA_ID.isnull()]
                    geocoded_props = df.dropna(subset=['MSA_ID'])

                    geocoded_props['AddDate'] = datetime.datetime.now().date()

                    run_macro_reports.run_macro_reports(geocoded_props, only_new_markets=True)

                    if update_db:
                        sql = sql_caller.SqlCaller()
                        sql.db_dump_props(df=geocoded_props,source='mls')


                    writer = pd.ExcelWriter('REDFIN_FailedGeocode.xlsx')
                    failed_geocoded_props.to_excel(writer, 'Sheet1', index=False)
                    writer.save()

    def run_MLS_audit(self, new_df):
        '''
            Checks to see any properties in Properties table already exists.
            This is to check if Gsheet has any MLS listed properties.
        '''
        sql = sql_caller.SqlCaller(create_tables=False)

        current_listings = sql.get_active_properties()

        common = new_df[(new_df.PropertyHashID.isin(current_listings.PropertyHashID))]

        if len(common) > 1:
            print('MLS listings already exists from Gsheet:')
            print(common['Address'])

        new_listings = new_df[(~new_df.PropertyHashID.isin(current_listings.PropertyHashID))]

        return new_listings

