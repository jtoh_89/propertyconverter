import censusgeocode as cg
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from dblayer import sql_caller
import hashlib
from mortgage import Loan
import datetime
import sys
from msa_reports import run_macro_reports
import os
import requests
import json
import geocoder

class PropertyParser():
    @staticmethod
    def run_property_conversion(df):
        sql = sql_caller.SqlCaller(create_tables=False)

        msa_counties = sql.get_msa_counties()
        msa_names = sql.get_msa_names()

        print('Processing addresses\n\n')
        for i, row in df.iterrows():
            count = 0
            while count < 4:
                if count < 2:
                    if 'USA' in row['Address']:
                        address = row['Address']
                    else:
                        address = row['Address'] + ', ' + row['City'] + ', ' + row['State'] + ' ' + str(row['Zip'])
                else:
                    address = row['Address'] + ', ' + row['City'] + ', ' + row['State']

                prop_dict = list(cg.onelineaddress(address))

                if len(row['Status']) < 1:
                    df.at[i, 'Status'] = 'Active'

                if len(prop_dict) > 1:
                    try:
                        if prop_dict[0]['geographies']['2010 Census Blocks'][0]['GEOID'] != prop_dict[1]['geographies']['2010 Census Blocks'][0]['GEOID']:
                            if abs(prop_dict[0]['coordinates']['x'] - prop_dict[1]['coordinates']['x']) < .001:
                                pass
                            else:
                                print('error geocoding address: {} - different geocodes for address'.format(address))
                                count +=1
                                continue
                    except:
                        print('Multiple Address for {}'.format(address))
                        count = 4
                        continue
                try:
                    # df.at[i, 'CensusGeoID'] = int(prop_dict[0]['geographies']['2010 Census Blocks'][0]['GEOID'])
                    df.at[i, 'State_ID'] = prop_dict[0]['geographies']['2010 Census Blocks'][0]['STATE']
                    df.at[i, 'County_UID'] = prop_dict[0]['geographies']['2010 Census Blocks'][0]['STATE'] + prop_dict[0]['geographies']['2010 Census Blocks'][0]['COUNTY']
                    df.at[i, 'Tract_ID'] = prop_dict[0]['geographies']['2010 Census Blocks'][0]['TRACT']
                    df.at[i, 'x_coordinate'] = prop_dict[0]['coordinates']['x']
                    df.at[i, 'y_coordinate'] = prop_dict[0]['coordinates']['y']


                    row['Price'] = int(str(row['Price']).replace(',','').replace('$','').replace('.',''))

                    if row['Price'] > 0 and row['Downpayment'] > 0:
                        df.at[i, 'MortgagePayment'] = Loan(principal=int(row['Price']) * (1-row['Downpayment']), interest=df.at[i, 'MortgageInterest']*.01, term=30).monthly_payment
                    else:
                        df.at[i, 'MortgagePayment'] = 0

                    df.at[i, 'UpdateDate'] = datetime.datetime.now().date()

                    try:
                        df.at[i, 'MSA_ID'] = msa_counties[df.at[i, 'County_UID']]
                        df.at[i, 'MSA_Name'] = msa_names[df.at[i, 'MSA_ID']].replace(',','').replace(' ','')
                        print('Completed: {}'.format(address))
                    except Exception:
                        print('No MSA_ID match for: {} (CountyUID: {})'.format(address, df.at[i, 'County_UID']))
                        break
                    break

                except Exception:
                    count += 1
                    if count == 4:
                        print('!!!!!FAIL!!!!!! from geocode data for {}. CHECK RESPONSE DATA'.format(address))
                        df = df[df['Address'] != row['Address']]
                        continue
                    continue

        print('Address Processing COMPLETE')
        return df

    @staticmethod
    def run_audit(new_df, audit_type, market=None):
        sql = sql_caller.SqlCaller(create_tables=False)

        if audit_type == 'mls':
            current_listings = sql.get_all_properties(source='mls',market=market)
        elif audit_type == 'prop_mls_sourced':
            current_listings = sql.get_all_properties(source='prop_mls_sourced')
            all_current_listings = sql.get_all_properties()
        elif audit_type == 'active':
            current_listings = sql.get_active_properties()
        elif audit_type == 'all':
            current_listings = sql.get_all_properties()
        else:
            current_listings = sql.get_all_properties(source='nonmls')


        if audit_type == 'prop_mls_sourced':
            if len(current_listings) < 1 and len(all_current_listings) < 1:
                return new_df
            all_common = new_df.merge(all_current_listings,on=['PropertyHashID','PropertyHashID'])
            mls_common = new_df.merge(current_listings,on=['PropertyHashID','PropertyHashID'])

            new_listings = new_df[(~new_df.PropertyHashID.isin(all_common.PropertyHashID))]

            inactive_listings = current_listings[(~current_listings.PropertyHashID.isin(mls_common.PropertyHashID))]

            new_status = all_common[all_common.Status_x!= all_common.Status_y]
            new_price = all_common[all_common.Price_x.astype(int) != all_common.Price_y.astype(int)]
            new_rent = all_common[all_common.Rent_x.astype(int) != all_common.Rent_y.astype(int)]
        else:
            if len(current_listings) < 1:
                return new_df


            common = new_df.merge(current_listings,on=['PropertyHashID','PropertyHashID'])
            new_listings = new_df[(~new_df.PropertyHashID.isin(common.PropertyHashID))]
            inactive_listings = current_listings[(~current_listings.PropertyHashID.isin(common.PropertyHashID))]
            new_status = common[common.Status_x!= common.Status_y]
            new_price = common[common.Price_x.astype(int) != common.Price_y.astype(int)]

            if audit_type != 'mls':
                new_rent = common[common.Rent_x.astype(int) != common.Rent_y.astype(int)]
            else:
                new_rent = pd.DataFrame()


        if not new_status.empty:
            if audit_type == 'mls':
                sql.db_update_mls_properties(updatetype='status', df=new_status)
            else:
                sql.db_update_properties(updatetype='status', df=new_status)
        if not new_price.empty:
            if audit_type == 'mls':
                sql.db_update_mls_properties(updatetype='price', df=new_price)
            else:
                sql.db_update_properties(updatetype='price', df=new_price)
        if not inactive_listings.empty:
            inactive_listings = inactive_listings[inactive_listings['Status'] != 'Inactive']

            if audit_type == 'mls':
                sql.db_update_mls_properties(updatetype='status_inactive', df=inactive_listings)
            else:
                sql.db_update_properties(updatetype='status_inactive', df=inactive_listings)
        if not new_rent.empty:
            if audit_type == 'mls':
                sql.db_update_mls_properties(updatetype='rent', df=new_rent)
            else:
                sql.db_update_properties(updatetype='rent', df=new_rent)

        return new_listings

    @staticmethod
    def generate_Pre_ESRI():
        path = os.path.dirname(os.path.abspath(__file__))
        path = path + '/processed_files'
        sql = sql_caller.SqlCaller()

        new_props = sql.get_new_properties_esri()

        if len(new_props) > 1:
            writer = pd.ExcelWriter(os.path.join(path, 'Pre_ESRI.xlsx'))
            new_props.to_excel(writer, 'Sheet1', index=False)
            writer.save()
        else:
            print('No new properties')

    @staticmethod
    def normalize_address(df):
        for i, row in df.iterrows():
            address_string = (row['Address'].strip() + ', ' + row['City'].strip() + ', ' + row['State'].strip() + ' ' + str(int(row['Zip'])).strip())
            gmap_api = 'AIzaSyDGiyj328qVAsJWBye7MJ-oEDVBkVg1gzU'

            address = geocoder.google(address_string, key=gmap_api)

            try:
                df.at[i, 'Address'] = address.current_result.address.split(',')[0].strip()
                df.at[i, 'City'] = address.current_result.address.split(',')[1].strip()
                df.at[i, 'State'] = address.current_result.address.split(',')[2].split()[0].strip()
                df.at[i, 'Zip'] = address.current_result.address.split(',')[2].split()[1].strip()
            except Exception as e:
                print('error for: {}'.format(e))
                continue

        return df

    @staticmethod
    def filter_ESRI_analysis():
        path = os.path.dirname(os.path.abspath(__file__))
        path = path + '/processed_files'

        for filename in os.listdir(path):
            if 'DS_Store' in filename or '.py' in filename:
                continue

            if 'Analysis' in filename:
                with open(os.path.join(path, filename)) as file:

                    df = pd.read_csv(file)
                    df = df.rename(columns={'Address or Place':'Address'})

                    df = df.dropna(subset=['PropertyHashID'])

                    df['2019 Owner Occupied HUs (%)'] = df['2019 Owner Occupied HUs (%)'].astype(str).map(lambda x: x.replace('%', '')).astype(float)
                    df['2019 Median Household Income'] = df['2019 Median Household Income'].astype(str).map(lambda x: x.replace('$', '').replace(',', '')).astype(int)


                    sql = sql_caller.SqlCaller()

                    df_update = df[['PropertyHashID']]

                    sql.db_update_properties(updatetype='esri_analyzed', df=df_update)


                    df = df[df['2019 Owner Occupied HUs (%)'] > 60]
                    df = df[df['2019 Median Household Income'] > 40000]
                    df = df[df['2019 Personal Crime Index'] < 350]

                    df = df.rename(columns={
                        '2019 Owner Occupied HUs (%)':'ESRI_OwnerOccupiedRates',
                        '2019 Median Household Income':'ESRI_MedianHouseholdIncome',
                        '2019 Personal Crime Index':'ESRI_PersonalCrimeIndex'})

                    df['GoodDeal'] = 1

                    sql.db_update_properties(updatetype='gooddeal',df=df)






