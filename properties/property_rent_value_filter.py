import os
import censusgeocode as cg
import pandas as pd
from dblayer import sql_caller
import sys
from properties import property_parser



class RentPriceFilter():

    @staticmethod
    def dump_decent_mls_props():
        sql = sql_caller.SqlCaller()
        df = sql.get_Active_MLS()


        df['RentToPriceRatio'] = ((df['Rent'] / df['Price']) * 100).round(2)

        # Update MLS_Properties with RentToPriceRatio values
        if not df.empty:
            sql.db_update_mls_properties(updatetype='rentprice', df=df)

        df = df[(df['RentToPriceRatio'] > .5) | (df['YearBuilt'] > 2000)]

        df = property_parser.PropertyParser().run_audit(new_df=df,audit_type='prop_mls_sourced')

        if len(df) > 0:
            sql.db_dump_props(df)
        else:
            print('No new MLS listings to add')


